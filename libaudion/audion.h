#ifndef AUDION_H
#define AUDION_H 1

/* TODO:
 *   Monitoring & Interception.
 *   Recording.
 *   Error reporting (I keep wanting to have 'CORBA_Environment *ev')
 *   Whatever else is in esd.h
 */

#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <glib.h>

typedef enum {
  AUD_ENCODING_PCM,
  AUD_ENCODING_GSM,
} AUDEncoding;

typedef struct {
  guint rate;
  AUDEncoding encoding;
  guint8 num_bits;
  guint8 num_channels;
  guint8 is_littleendian;
} AUDFormat;

typedef struct timeval AUDTime;

typedef struct {
  enum { AUD_POS_SAMPLES, AUD_POS_TIME } type;
  enum { AUD_POS_RELATIVE, AUD_POS_ABSOLUTE } whence;
  union {
    AUDTime pos_time;
    guint64 pos_samples;
  } u;
} AUDPosition;

typedef struct { /* Every audion connection is uniquely identified by its [program_id, pid, hostid, cnxid] */
  char *program_name;
  int pid, hostid, cnxid;
} AUDConnectionInfo;

typedef struct _AUDConnection AUDConnection;

AUDConnection *audion_connection_open(const char *host, const char *program_name);
void audion_connection_close(AUDConnection *cnx);
void audion_connection_list_connections(AUDConnection *cnx, AUDConnectionInfo **cnx_infos);
void audion_connection_list_samples(AUDConnection *cnx, char ***sample_names);
void audion_connection_get_position(AUDConnection *cnx, AUDPosition *curpos /* curpos->type should be set */);
AUDConnectionInfo *audion_connection_get_info(AUDConnection *cnx);

typedef struct _AUDStream AUDStream;
AUDStream *audion_stream_open(AUDConnection *cnx);
void audion_stream_play(AUDStream *stream, AUDPosition *start_play, AUDFormat *sample_format,
			gpointer sample_data, guint sample_len);
#if 0
/* Recording & playback streams should really be totally separate, I think. */
/* Do we want this to be blocking or non-blocking? */
void audion_stream_record(AUDStream *stream, AUDPosition *start_record, AUDFormat *sample_format,
			  gpointer sample_data, guint sample_len);
#endif
void audion_stream_get_position(AUDStream *stream, AUDPosition *curpos /* curpos->type and curpos->whence should be set */); 
void audion_stream_close(AUDStream *stream);

typedef struct _AUDSample AUDSample;
typedef struct {
  guint8 looping : 1;
  guint8 stop_previous : 1;
} AUDSamplePlayOptions;

AUDSample *audion_sample_create(AUDConnection *cnx, const char *name, AUDFormat *sample_format, gpointer sample_data);
AUDSample *audion_sample_attach(AUDConnection *cnx, const char *name);
void audion_sample_play(AUDSample *sample, AUDPosition *start_play, AUDSamplePlayOptions *opts);
void audion_sample_stop(AUDSample *sample, gboolean finish_loop);
void audion_sample_remove(AUDSample *sample); /* Removes it from server, and frees */
void audion_sample_free(AUDSample *sample); /* Just frees here, leaves it on the server */

#endif
